package ru.day.ltd.catalog.service.area;

import ru.day.ltd.catalog.dto.AddressDTO;
import ru.day.ltd.catalog.dto.AreaDTO;

import java.util.ArrayList;
import java.util.List;

public class AreaServiceIml implements AreaService{

    private List<AreaDTO> areaDTOS;

    public AreaServiceIml() {
        areaDTOS = new ArrayList<>();
    }

    @Override
    public void addItem(AreaDTO areaDTO) {
        areaDTOS.add(areaDTO);
    }

    @Override
    public void updateItem(int index, AreaDTO updatedAreaDTO) {
        if (index >= 0 && index < areaDTOS.size()) {
            areaDTOS.set(index, updatedAreaDTO);
        } else {
            throw new IndexOutOfBoundsException("Invalid index");
        }
    }

    @Override
    public void deleteItem(int index) {
        if (index >= 0 && index < areaDTOS.size()) {
            areaDTOS.remove(index);
        } else {
            throw new IndexOutOfBoundsException("Invalid index");
        }
    }

    @Override
    public List<AreaDTO> getAllItems() {
        return areaDTOS;
    }
}
