package ru.day.ltd.catalog.service.tag;


import ru.day.ltd.catalog.dto.TagDTO;

import java.util.List;

public interface TagService {

    void addItem(TagDTO tagDTO);

    void updateItem(int index, TagDTO updatedTagDTO);

    void deleteItem(int index);

    List<TagDTO> getAllItems();
}
