package ru.day.ltd.catalog.service.itemcatalog;

import ru.day.ltd.catalog.dto.ItemCatalogDTO;

import java.util.List;

public interface ItemCatalogService {

    void addItem(ItemCatalogDTO itemCatalogDTO);

    void updateItem(int index, ItemCatalogDTO updatedItemCatalogDTO);

    void deleteItem(int index);

    List<ItemCatalogDTO> getAllItems();
}
