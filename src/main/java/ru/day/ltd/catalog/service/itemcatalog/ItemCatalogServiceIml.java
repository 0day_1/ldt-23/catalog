package ru.day.ltd.catalog.service.itemcatalog;

import ru.day.ltd.catalog.dto.ItemCatalogDTO;

import java.util.ArrayList;
import java.util.List;

public class ItemCatalogServiceIml implements ItemCatalogService{

    private List<ItemCatalogDTO> itemCatalogDTOs;

    public ItemCatalogServiceIml() {
        itemCatalogDTOs = new ArrayList<>();
    }

    @Override
    public void addItem(ItemCatalogDTO itemCatalogDTO) {
        itemCatalogDTOs.add(itemCatalogDTO);
    }

    @Override
    public void updateItem(int index, ItemCatalogDTO updatedItemCatalogDTO) {
        if (index >= 0 && index < itemCatalogDTOs.size()) {
            itemCatalogDTOs.set(index, updatedItemCatalogDTO);
        } else {
            throw new IndexOutOfBoundsException("Invalid index");
        }
    }

    @Override
    public void deleteItem(int index) {
        if (index >= 0 && index < itemCatalogDTOs.size()) {
            itemCatalogDTOs.remove(index);
            // Дополнительные действия по удалению элемента
        } else {
            throw new IndexOutOfBoundsException("Invalid index");
        }
    }

    @Override
    public List<ItemCatalogDTO> getAllItems() {
        return itemCatalogDTOs;
    }
}
