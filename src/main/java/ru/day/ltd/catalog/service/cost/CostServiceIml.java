package ru.day.ltd.catalog.service.cost;

import ru.day.ltd.catalog.dto.CostDTO;

import java.util.ArrayList;
import java.util.List;

public class CostServiceIml implements CostService{

    private List<CostDTO> costDTOS;

    public CostServiceIml() {
        costDTOS = new ArrayList<>();
    }

    @Override
    public void addItem(CostDTO costDTO) {
        costDTOS.add(costDTO);
    }

    @Override
    public void updateItem(int index, CostDTO updatedCostDTO) {
        if (index >= 0 && index < costDTOS.size()) {
            costDTOS.set(index, updatedCostDTO);
        } else {
            throw new IndexOutOfBoundsException("Invalid index");
        }
    }

    @Override
    public void deleteItem(int index) {
        if (index >= 0 && index < costDTOS.size()) {
            costDTOS.remove(index);
        } else {
            throw new IndexOutOfBoundsException("Invalid index");
        }
    }

    @Override
    public List<CostDTO> getAllItems() {
        return costDTOS;
    }
}
