package ru.day.ltd.catalog.service.tag;

import ru.day.ltd.catalog.dto.TagDTO;

import java.util.ArrayList;
import java.util.List;

public class TagServiceIml implements TagService{

    private List<TagDTO> tagDTOS;

    public TagServiceIml() {
        tagDTOS = new ArrayList<>();
    }

    @Override
    public void addItem(TagDTO tagDTO) {
        tagDTOS.add(tagDTO);
    }

    @Override
    public void updateItem(int index, TagDTO updatedTagDTO) {
        if (index >= 0 && index < tagDTOS.size()) {
            tagDTOS.set(index, updatedTagDTO);
        } else {
            throw new IndexOutOfBoundsException("Invalid index");
        }
    }

    @Override
    public void deleteItem(int index) {
        if (index >= 0 && index < tagDTOS.size()) {
            tagDTOS.remove(index);
        } else {
            throw new IndexOutOfBoundsException("Invalid index");
        }
    }

    @Override
    public List<TagDTO> getAllItems() {
        return tagDTOS;
    }
}
