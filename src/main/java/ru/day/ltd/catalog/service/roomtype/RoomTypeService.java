package ru.day.ltd.catalog.service.roomtype;

import ru.day.ltd.catalog.dto.RoomTypeDTO;

import java.util.List;

public interface RoomTypeService {

    void addItem(RoomTypeDTO roomTypeDTO);

    void updateItem(int index, RoomTypeDTO updatedRoomTypeDTO);

    void deleteItem(int index);

    List<RoomTypeDTO> getAllItems();
}
