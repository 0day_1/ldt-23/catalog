package ru.day.ltd.catalog.service.area;

import ru.day.ltd.catalog.dto.AreaDTO;

import java.util.List;

public interface AreaService {

    void addItem(AreaDTO areaDTO);

    void updateItem(int index, AreaDTO updatedAreaDTO);

    void deleteItem(int index);

    List<AreaDTO> getAllItems();
}
