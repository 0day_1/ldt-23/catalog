package ru.day.ltd.catalog.service.address;

import ru.day.ltd.catalog.dto.AddressDTO;

import java.util.ArrayList;
import java.util.List;

public class AddressServiceIml implements AddressService{

    private List<AddressDTO> addressDTOS;

    public AddressServiceIml() {
        addressDTOS = new ArrayList<>();
    }

    @Override
    public void addItem(AddressDTO addressDTO) {
        addressDTOS.add(addressDTO);
    }

    @Override
    public void updateItem(int index, AddressDTO updatedAddressDTO) {
        if (index >= 0 && index < addressDTOS.size()) {
            addressDTOS.set(index, updatedAddressDTO);
        } else {
            throw new IndexOutOfBoundsException("Invalid index");
        }
    }

    @Override
    public void deleteItem(int index) {
        if (index >= 0 && index < addressDTOS.size()) {
            addressDTOS.remove(index);
        } else {
            throw new IndexOutOfBoundsException("Invalid index");
        }
    }

    @Override
    public List<AddressDTO> getAllItems() {
        return addressDTOS;
    }
}
