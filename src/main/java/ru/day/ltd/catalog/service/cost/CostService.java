package ru.day.ltd.catalog.service.cost;

import ru.day.ltd.catalog.dto.CostDTO;

import java.util.List;

public interface CostService {

    void addItem(CostDTO costDTO);

    void updateItem(int index, CostDTO updatedCostDTO);

    void deleteItem(int index);

    List<CostDTO> getAllItems();
}
