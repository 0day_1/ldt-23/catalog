package ru.day.ltd.catalog.service.file;

import ru.day.ltd.catalog.dto.FileDTO;

import java.util.ArrayList;
import java.util.List;

public class FileServiceIml implements FileService{

    private List<FileDTO> fileDTOS;

    public FileServiceIml() {
        fileDTOS = new ArrayList<>();
    }

    @Override
    public void addItem(FileDTO fileDTO) {
        fileDTOS.add(fileDTO);
    }

    @Override
    public void updateItem(int index, FileDTO updatedFileDTO) {
        if (index >= 0 && index < fileDTOS.size()) {
            fileDTOS.set(index, updatedFileDTO);
        } else {
            throw new IndexOutOfBoundsException("Invalid index");
        }
    }

    @Override
    public void deleteItem(int index) {
        if (index >= 0 && index < fileDTOS.size()) {
            fileDTOS.remove(index);
        } else {
            throw new IndexOutOfBoundsException("Invalid index");
        }
    }

    @Override
    public List<FileDTO> getAllItems() {
        return fileDTOS;
    }
}
