package ru.day.ltd.catalog.service.roomtype;

import ru.day.ltd.catalog.dto.RoomTypeDTO;

import java.util.ArrayList;
import java.util.List;

public class RoomTypeServiceIml implements RoomTypeService{

    private List<RoomTypeDTO> roomTypeDTOS;

    public RoomTypeServiceIml() {
        roomTypeDTOS = new ArrayList<>();
    }

    @Override
    public void addItem(RoomTypeDTO roomTypeDTO) {
        roomTypeDTOS.add(roomTypeDTO);
    }

    @Override
    public void updateItem(int index, RoomTypeDTO updatedRoomTypeDTO) {
        if (index >= 0 && index < roomTypeDTOS.size()) {
            roomTypeDTOS.set(index, updatedRoomTypeDTO);
        } else {
            throw new IndexOutOfBoundsException("Invalid index");
        }
    }

    @Override
    public void deleteItem(int index) {
        if (index >= 0 && index < roomTypeDTOS.size()) {
            roomTypeDTOS.remove(index);
        } else {
            throw new IndexOutOfBoundsException("Invalid index");
        }
    }

    @Override
    public List<RoomTypeDTO> getAllItems() {
        return roomTypeDTOS;
    }
}
