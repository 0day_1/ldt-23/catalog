package ru.day.ltd.catalog.service.address;

import ru.day.ltd.catalog.dto.AddressDTO;

import java.util.List;

public interface AddressService {

    void addItem(AddressDTO addressDTO);

    void updateItem(int index, AddressDTO updatedAddressDTO);

    void deleteItem(int index);

    List<AddressDTO> getAllItems();
}
