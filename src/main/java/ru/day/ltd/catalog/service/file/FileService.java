package ru.day.ltd.catalog.service.file;

import ru.day.ltd.catalog.dto.FileDTO;

import java.util.List;

public interface FileService {

    void addItem(FileDTO fileDTO);

    void updateItem(int index, FileDTO updatedFileDTO);

    void deleteItem(int index);

    List<FileDTO> getAllItems();
}
