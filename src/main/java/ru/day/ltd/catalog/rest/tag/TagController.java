package ru.day.ltd.catalog.rest.tag;

import org.springframework.web.bind.annotation.*;
import ru.day.ltd.catalog.dto.TagDTO;
import ru.day.ltd.catalog.service.tag.TagService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/tag/")
public class TagController {

    private TagService tagService;

    @PostMapping("add")
    private String add(
            @RequestBody TagDTO tagDTO
    ) {
        tagService.addItem(tagDTO);
        return "Добавлено";
    }

    @PostMapping("update")
    private String update(
            @RequestBody TagDTO tagDTO
    ) {
        tagService.updateItem(0, tagDTO);
        return "Обновлено";
    }

    @DeleteMapping("delete")
    private String delete() {
        tagService.deleteItem(0);
        return "Удалено";
    }

    @GetMapping("getAllItems")
    private List<TagDTO> getAllItems() {
        return tagService.getAllItems();
    }

    @GetMapping("find")
    private List<TagDTO> find(String id) {
        return tagService.getAllItems();
    }
}
