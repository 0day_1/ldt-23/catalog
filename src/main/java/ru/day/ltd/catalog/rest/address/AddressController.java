package ru.day.ltd.catalog.rest.address;

import org.springframework.web.bind.annotation.*;
import ru.day.ltd.catalog.dto.AddressDTO;
import ru.day.ltd.catalog.service.address.AddressService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/address/")
public class AddressController {

    private AddressService addressService;

    @PostMapping("add")
    private String add(
            @RequestBody AddressDTO addressDTO
            ) {
        addressService.addItem(addressDTO);
        return "Добавлено";
    }

    @PostMapping("update")
    private String update(
            @RequestBody AddressDTO addressDTO
    ) {
        addressService.updateItem(0, addressDTO);
        return "Обновлено";
    }

    @DeleteMapping("delete")
    private String delete() {
        addressService.deleteItem(0);
        return "Удалено";
    }

    @GetMapping("getAllItems")
    private List<AddressDTO> getAllItems() {
        return addressService.getAllItems();
    }

    @GetMapping("find")
    private List<AddressDTO> find(String id) {
        return addressService.getAllItems();
    }
}
