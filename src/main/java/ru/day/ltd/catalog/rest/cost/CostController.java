package ru.day.ltd.catalog.rest.cost;

import org.springframework.web.bind.annotation.*;
import ru.day.ltd.catalog.dto.CostDTO;
import ru.day.ltd.catalog.service.cost.CostService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/cost/")
public class CostController {

    private CostService costService;

    @PostMapping("add")
    private String add(
            @RequestBody CostDTO costDTO
    ) {
        costService.addItem(costDTO);
        return "Добавлено";
    }

    @PostMapping("update")
    private String update(
            @RequestBody CostDTO costDTO
    ) {
        costService.updateItem(0, costDTO);
        return "Обновлено";
    }

    @DeleteMapping("delete")
    private String delete() {
        costService.deleteItem(0);
        return "Удалено";
    }

    @GetMapping("getAllItems")
    private List<CostDTO> getAllItems() {
        return costService.getAllItems();
    }

    @GetMapping("find")
    private List<CostDTO> find(String id) {
        return costService.getAllItems();
    }
}
