package ru.day.ltd.catalog.rest.itemcatalog;

import org.springframework.web.bind.annotation.*;
import ru.day.ltd.catalog.dto.ItemCatalogDTO;
import ru.day.ltd.catalog.service.itemcatalog.ItemCatalogService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/itemcatalog/")
public class ItemCatalogController {

    private ItemCatalogService itemCatalogService;

    @PostMapping("add")
    private String add(
            @RequestBody ItemCatalogDTO itemCatalogDTO
    ) {
        itemCatalogService.addItem(itemCatalogDTO);
        return "Добавлено";
    }

    @PostMapping("update")
    private String update(
            @RequestBody ItemCatalogDTO itemCatalogDTO
    ) {
        itemCatalogService.updateItem(0, itemCatalogDTO);
        return "Обновлено";
    }

    @DeleteMapping("delete")
    private String delete() {
        itemCatalogService.deleteItem(0);
        return "Удалено";
    }

    @GetMapping("getAllItems")
    private List<ItemCatalogDTO> getAllItems() {
        return itemCatalogService.getAllItems();
    }

    @GetMapping("find")
    private List<ItemCatalogDTO> find(String id) {
        return itemCatalogService.getAllItems();
    }
}
