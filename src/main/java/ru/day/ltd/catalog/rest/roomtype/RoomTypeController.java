package ru.day.ltd.catalog.rest.roomtype;

import org.springframework.web.bind.annotation.*;
import ru.day.ltd.catalog.dto.RoomTypeDTO;
import ru.day.ltd.catalog.service.roomtype.RoomTypeService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/roomtype/")
public class RoomTypeController {

    private RoomTypeService roomTypeService;

    @PostMapping("add")
    private String add(
            @RequestBody RoomTypeDTO roomTypeDTO
    ) {
        roomTypeService.addItem(roomTypeDTO);
        return "Добавлено";
    }

    @PostMapping("update")
    private String update(
            @RequestBody RoomTypeDTO roomTypeDTO
    ) {
        roomTypeService.updateItem(0, roomTypeDTO);
        return "Обновлено";
    }

    @DeleteMapping("delete")
    private String delete() {
        roomTypeService.deleteItem(0);
        return "Удалено";
    }

    @GetMapping("getAllItems")
    private List<RoomTypeDTO> getAllItems() {
        return roomTypeService.getAllItems();
    }

    @GetMapping("find")
    private List<RoomTypeDTO> find(String id) {
        return roomTypeService.getAllItems();
    }
}
