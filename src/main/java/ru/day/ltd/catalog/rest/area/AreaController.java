package ru.day.ltd.catalog.rest.area;

import org.springframework.web.bind.annotation.*;
import ru.day.ltd.catalog.dto.AreaDTO;
import ru.day.ltd.catalog.service.area.AreaService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/area/")
public class AreaController {

    private AreaService areaService;

    @PostMapping("add")
    private String add(
            @RequestBody AreaDTO areaDTO
    ) {
        areaService.addItem(areaDTO);
        return "Добавлено";
    }

    @PostMapping("update")
    private String update(
            @RequestBody AreaDTO areaDTO
    ) {
        areaService.updateItem(0, areaDTO);
        return "Обновлено";
    }

    @DeleteMapping("delete")
    private String delete() {
        areaService.deleteItem(0);
        return "Удалено";
    }

    @GetMapping("getAllItems")
    private List<AreaDTO> getAllItems() {
        return areaService.getAllItems();
    }

    @GetMapping("find")
    private List<AreaDTO> find(String id) {
        return areaService.getAllItems();
    }
}
