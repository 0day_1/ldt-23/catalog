package ru.day.ltd.catalog.rest.file;

import org.springframework.web.bind.annotation.*;
import ru.day.ltd.catalog.dto.FileDTO;
import ru.day.ltd.catalog.service.file.FileService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/file/")
public class FileController {

    private FileService fileService;

    @PostMapping("add")
    private String add(
            @RequestBody FileDTO fileDTO
    ) {
        fileService.addItem(fileDTO);
        return "Добавлено";
    }

    @PostMapping("update")
    private String update(
            @RequestBody FileDTO fileDTO
    ) {
        fileService.updateItem(0, fileDTO);
        return "Обновлено";
    }

    @DeleteMapping("delete")
    private String delete() {
        fileService.deleteItem(0);
        return "Удалено";
    }

    @GetMapping("getAllItems")
    private List<FileDTO> getAllItems() {
        return fileService.getAllItems();
    }

    @GetMapping("find")
    private List<FileDTO> find(String id) {
        return fileService.getAllItems();
    }
}
