package ru.day.ltd.catalog.dto;

public class TagDTO {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

