package ru.day.ltd.catalog.dto;

import java.util.List;

public class ItemCatalogDTO {
    private String name;
    private AddressDTO address;
    private String description;
    private CostDTO cost;
    private List<FileDTO> fileList;
    private List<TagDTO> tags;
    private RoomTypeDTO roomType;
    private AreaDTO area;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CostDTO getCost() {
        return cost;
    }

    public void setCost(CostDTO cost) {
        this.cost = cost;
    }

    public List<FileDTO> getFileList() {
        return fileList;
    }

    public void setFileList(List<FileDTO> fileList) {
        this.fileList = fileList;
    }

    public List<TagDTO> getTags() {
        return tags;
    }

    public void setTags(List<TagDTO> tags) {
        this.tags = tags;
    }

    public RoomTypeDTO getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomTypeDTO roomType) {
        this.roomType = roomType;
    }

    public AreaDTO getArea() {
        return area;
    }

    public void setArea(AreaDTO area) {
        this.area = area;
    }
}

